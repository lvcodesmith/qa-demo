package com.example
import geb.spock.GebSpec
/**
 * Created by Scott on 2/23/2016.
 */
class ExampleSpec extends GebSpec {
    def "searching for lolcats shows lolcat results"() {
        given:
        go "https://google.com"
        when:
        $("input[name=q]").value("lolcats")
        and:
        waitFor(5) {
            $("#center_col").isDisplayed()
        }
        then:
        "icanhas.cheezburger.com/lolcats" == $("cite._Rm", 0).text()
    }
}